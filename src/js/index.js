//Configurando chaves e url da API Marvel
var PUBLIC_KEY  	= "9e4762533b72e7179857f761780ced71";
var PRIVATE_KEY 	= "cf2be0e810bce64632cfeac3f25e8ba6c51d373f";
var TS 				= "1"; 
var HASH 			= "170139cd69bb65ca6aeb594bb5a00905";
var url 			= "https://gateway.marvel.com/v1/public/comics?ts="+TS+"&apikey="+PUBLIC_KEY+"&hash="+HASH;

//Filtro da página
const REGISTROS		= 10;

//Arrays
var thead			= new Array();
var personagens		= new Array();
var quadrinhos		= [];
var paginas;

//Criando tabela
var tabela			= criaELemento("table");
tabela.className	= "table table-responsive text-center";
tabela.id			= "tabela";
document.getElementById('card').appendChild(tabela);

//Ao carregar a página, fazer requisição ajax
window.onload = function() {

	$.ajax({
		method: "GET",
		url: url,
  		data: {
    		limit: 50
		},
		contentType: "application/json; charset=utf-8",
		beforeSend: function(){
    		document.getElementById('tabela').innerHTML = '<p class="text-center display-4">Carregando...</p>';
		},
	    success: function(msg){
    		document.getElementById('tabela').innerHTML = '';

			//Calculo para o total de paginas
			paginas = msg.data.results.length/REGISTROS;

			//Separação dos personagens e quadrinhos para pesquisa
    		let cont1 = 0;
    		for (let j = 0; j < msg.data.results.length; j++) {   		
	    		for (let i = 0; i < msg.data.results[j].characters.items.length; i++) {
	    			if (msg.data.results[j].characters.items.length != 0) {
	    				personagens[cont1] = msg.data.results[j].characters.items[i].name;
	    				quadrinhos[cont1]  = msg.data.results[j];
	    			}else{
						personagens[cont1] = msg.data.results[j].series.name;
	    				quadrinhos[cont1]  = msg.data.results[j];
	    			}
	    			cont1++;
	    		}
    		}	

			//Montar a tabela dinamicamente
    		let cont   = 0;
			for (let j = 0 ; j < paginas ; j++) {
				thead	   = criaELemento("thead");
				pagina 	   = "pagina"+(j+1);
				thead.id   = pagina;
				for (let i = 0 ; i < msg.data.results.length/paginas ; i++) {
					tr  = criaELemento("tr");
					th1 = criaELemento("th");
					th2 = criaELemento("th");
					th3 = criaELemento("th");

					img = criaELemento("img");
					p1  = criaELemento("p");
					p2  = criaELemento("p");
					a   = criaELemento("a");

					th1.className = "thumbnail";
					th2.className = "thumbnail";
					th3.id 		  = "button"; 

					img.id  = "thumbnail";
					img.src = msg.data.results[cont].thumbnail.path+"."+msg.data.results[cont].thumbnail.extension;

					p1.className   = "titulo";
					
					if (msg.data.results[cont].creators.items[cont] != undefined) {
						p1.textContent = msg.data.results[cont].title;
						p2.textContent = "Autor: "+msg.data.results[cont].creators.items[cont].name;
				    }else{
				    	p1.textContent = msg.data.results[cont].title;
				    }

					a.target   		= "_blank";
					a.href 	   		= msg.data.results[i].urls[0].url;
					a.className		= "btn btn-outline-primary";
					a.textContent	= "Ver mais";

					th3.appendChild(a);
					th2.appendChild(p1);
					th2.appendChild(p2);
					th1.appendChild(img);
					tr.appendChild(th1);
					tr.appendChild(th2);
					tr.appendChild(th3);
					thead.appendChild(tr);
					cont++;
				}
				document.getElementById('tabela').appendChild(thead);
			}
			//Chamada de funções para a paginação
    		montarPaginacao();
    		mostrarPagina();  

			//Nomes de pernogens para a pesquisa  
    		console.log(personagens);	
    	}
	});

}

//Função para criar elementos
function criaELemento(elemento) 
{
	return document.createElement(elemento);
}

//Função para ajeitar os dados na paginação
function mostrarPagina (e)
{
	let num = location.search.substring(1, location.search.length);
	let flag = false;
	for (let i = 0; i < paginas; i++) {
		comp = i+1;
		if ((comp != num) && (num != '') || (comp != 1) && (num == '')) {
			document.getElementById('pagina'+comp).style.display="none";
		}
		if ((comp == num) && (flag != true)) {
			pg = document.getElementById('pagina'+comp);
			pg.style.display="block";
			flag = true;
		}
	}
}

//Função para montar paginação
function montarPaginacao () 
{
	nav	  	       = criaELemento("nav");
	ul 	  	  	   = criaELemento("ul");
	ul.className   = "pagination";
	document.getElementById('card').appendChild(nav);
	nav.appendChild(ul);

	for (let i = 0; i < paginas; i++) {
		index = i+1
		li    = criaELemento("li"); 
		a     = criaELemento("a");

		li.class 	    = "page-item";
		a.className	 	= "page-link";
		a.textContent   = ""+index+"";
		a.href			= "index.html?"+index;
		a.onclick		= function () {mostrarPagina(paginas)};
		a.id 			= "link"+index;
		li.appendChild(a); 
		ul.appendChild(li);
	}
}	

//Função para buscar personagens e mostrar quadrinhos
function buscaPersonagem()
{
	let letra  = document.getElementById('buscaPersonagens').value;
	let flag   = true;
	var indice = [];
	let cont   = 0;
	let ref	   = 0;
	for (let i = 0 ; i < personagens.length; i++) {
		if (letra == personagens[i]) {
			indice[ref] = i;
			ref++;
			flag = false;
		}
	}
	if (document.getElementById('pagina'+paginas+1) != null) {
		table = document.getElementById('pagina'+paginas+1);
		if (table.parentNode) {
			document.getElementById('tabela').removeChild(table);
		}
	}
	if (document.getElementById('erro') != null) {
		erro = document.getElementById('erro');
		if (erro.parentNode) {
			document.getElementById('tabela').removeChild(erro);
		}
	}

	if (flag == false) {
		for (let j = 1; j <= paginas; j++) {
			document.getElementById('pagina'+j).style.display="none";
		}
		thead	   = criaELemento("thead");
		thead.id   = "pagina"+paginas+1;

		for (let i = 0 ; i < indice.length ; i++) {
			tr  = criaELemento("tr");
			th1 = criaELemento("th");
			th2 = criaELemento("th");
			th3 = criaELemento("th");

			img = criaELemento("img");
			p1  = criaELemento("p");
			p2  = criaELemento("p");
			a   = criaELemento("a");

			th1.className = "thumbnail";
			th2.className = "thumbnail";
			th3.id 		  = "button"; 
			img.id  = "thumbnail";
			img.src = quadrinhos[indice[i]].thumbnail.path+"."+quadrinhos[indice[i]].thumbnail.extension;

			p1.className   = "titulo";
			if (quadrinhos[indice[i]].creators.items[cont] != undefined) {
				p1.textContent = quadrinhos[cont].title;
				p2.textContent = "Autor: "+quadrinhos[indice[i]].creators.items[cont].name;
		    }else{
		    	p1.textContent = quadrinhos[indice[i]].title;
		    }

			a.target   		= "_blank";
			a.href 	   		= quadrinhos[indice[i]].urls[0].url;
			a.className		= "btn btn-outline-primary";
			a.textContent	= "Ver mais";

			th3.appendChild(a);
			th2.appendChild(p1);
			th2.appendChild(p2);
			th1.appendChild(img);
			tr.appendChild(th1);
			tr.appendChild(th2);
			tr.appendChild(th3);
			thead.appendChild(tr);
			cont++;
		}
		document.getElementById('tabela').appendChild(thead);
	}else{
		for (let j = 1; j <= paginas; j++) {
			document.getElementById('pagina'+j).style.display="none";
		}
		p  			  = criaELemento("p");
		p.id 		  = "erro";
		p.className   = "text-center display-4";
		p.textContent = "Personagem não encontrado";
		document.getElementById('tabela').appendChild(p);
	}
}